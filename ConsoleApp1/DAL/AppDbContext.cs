using System.Linq;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public DbSet<Game> Games { get; set; } = null!;
        public DbSet<Options> Options { get; set; } = null!;
        public DbSet<Player> Players { get; set; } = null!;
        public DbSet<BoatOptions> BoatOptions { get; set; } = null!;
        public DbSet<Boat> Boats { get; set; } = null!;

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
            
        }

        private static readonly ILoggerFactory LoggerFactory = Microsoft.Extensions.Logging.LoggerFactory.Create(
            builder =>
            {
                builder.AddFilter("Microsoft", LogLevel.Information)
                    .AddConsole();
            });

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder
                .UseLoggerFactory(LoggerFactory)
                .EnableSensitiveDataLogging()
                .UseSqlServer(
                    @"
                    Server=barrel.itcollege.ee,1533; 
                    User Id=student;
                    Password=Student.Bad.password.0; 
                    Database=alpha.akaver.com;
                    MultipleActiveResultSets=true"
                );
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<Player>()
                .HasOne<Game>()
                .WithOne(x => x.PlayerA)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey<Game>(x => x.PlayerAId);
            
            modelBuilder
                .Entity<Player>()
                .HasOne<Game>()
                .WithOne(x => x.PlayerB)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey<Game>(x => x.PlayerBId);

            foreach (var relationship in modelBuilder.Model
                .GetEntityTypes()
                .Where(e => !e.IsOwned())
                .SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Cascade;
            }
        }
    }
}