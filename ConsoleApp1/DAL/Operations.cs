﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Microsoft.EntityFrameworkCore;
using BoatOptions = Domain.BoatOptions;

namespace DAL
{
    public class Operations
    {
        public DbContextOptions<AppDbContext> DbOptions { get; }
        public Operations()
        {
            DbOptions = new DbContextOptionsBuilder<AppDbContext>().UseSqlServer(
                @"
                    Server=barrel.itcollege.ee,1533; 
                    User Id=student;
                    Password=Student.Bad.password.0; 
                    Database=alpha.akaver.com;
                    MultipleActiveResultSets=true"
            ).Options;
        }
        
        public void CreateSave(int id, AppDbContext context)
        {
        }

        public void OverwriteSave(int id, AppDbContext context)
        {
        }

        public Game LoadGame(int id)
        {
            using var dbContext = new AppDbContext(DbOptions);
            
            var save = dbContext.Games
                .Include(o => o.Options)
                .Include(b => b.Board)
                .Include(p => p.PlayerB)
                .Include(p => p.PlayerA)
                .Include(b => b.Options.BoatOptions)
                .Where(g => g.GameId == id).FirstOrDefaultAsync().Result;
            
            List<BoatOptions> boatOptionsList = new();
            
            foreach (var boatOptions in dbContext.BoatOptions
                .Include(b => b.Boat)
                .Include(o => o.Options)
                .Where(b => b.OptionsId == save.Options.OptionsId))
            {
                if (boatOptions.OptionsId == save.OptionsId)
                {
                    boatOptionsList.Add(boatOptions);
                }
            }

            save.Options.BoatOptions = boatOptionsList;
            
            return save;
        }

        public void DeleteSave(int id, AppDbContext context)
        {
        }
    }
}