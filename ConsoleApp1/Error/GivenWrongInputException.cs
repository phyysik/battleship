﻿using System;

namespace Error
{
    public class GivenWrongInputException : Exception
    {

        public GivenWrongInputException()
        {
        }

        public GivenWrongInputException(string message)
            : base(message)
        {
        }
        
    }
}