﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp1.Pages
{
    public class Boats : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public Boats(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<BoatOptions> BoatOptions { get; set; } = null!;

        public async Task OnGetAsync(int id)
        {
            BoatOptions = (IList<BoatOptions>) _context.Games
                .Where(g => g.GameId == id)
                .Include(g => g.Options).FirstOrDefaultAsync()
                .Result.Options.BoatOptions;
        }
    }
}