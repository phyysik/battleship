﻿using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Domain.Enums;
using Head;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace WebApp1.Pages
{
    public class NewGame : PageModel
    {
        private readonly AppDbContext _context;
        private readonly ILogger<NewGame> _logger;

        public NewGame(AppDbContext context, ILogger<NewGame> logger)
        {
            _context = context;
            _logger = logger;
        }

        [BindProperty] public string P1 { get; set; } = "Mangija 1";
        [BindProperty] public string P2 { get; set; } = "Mangija 2";
        [BindProperty] public int BoardWid { get; set; } = 10;
        [BindProperty] public int BoardHei { get; set; } = 10;
        [BindProperty] public EBoatContact BoatContact { get; set; } = EBoatContact.No;
        [BindProperty] public bool HitExtendsTurn { get; set; } = true;
        [BindProperty] public EPlayerType P2PlayerType { get; set; } = EPlayerType.Human;
        [BindProperty] public bool ShipGenerator { get; set; } = false;
        private Battleship? Game { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id = null)
        {
            if (id == null) return Page();
            
            var save = await _context.Games
                .Where(g => g.GameId == id)
                .Include(o => o.Options)
                .Include(g => g.PlayerA)
                .Include(g => g.PlayerB)
                .FirstOrDefaultAsync();

            P1 = save.PlayerA.Name;
            P2 = save.PlayerB.Name;
            BoardWid = save.Options.BoardWidth;
            BoardHei = save.Options.BoardHeight;
            BoatContact = save.Options.BoatContact;
            HitExtendsTurn = save.Options.ContinueAfterGoodBomb == EContinueAfterGoodBomb.True;
            P2PlayerType = save.Options.PvP ? EPlayerType.Human : EPlayerType.Ai;
            
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            var bs = new Battleship(new GameOptions(BoardWid, BoardHei, HitExtendsTurn, (BoatContact) BoatContact,
                P2PlayerType == EPlayerType.Human, new Head.BoatOptions()));
            
            if (ShipGenerator) bs.PlaceShipsRandomly();
            bs.SwitchLever();

            if (ShipGenerator || P2PlayerType == EPlayerType.Ai) bs.PlaceShipsRandomly();
            bs.SwitchLever();
            
            var serializedGame = bs.GetSerializedGameState();
            
            var options = new Options
            {
                Name = "Test",
                BoardWidth = BoardWid,
                BoardHeight = BoardHei,
                BoatContact = BoatContact,
                ContinueAfterGoodBomb = HitExtendsTurn ? EContinueAfterGoodBomb.True : EContinueAfterGoodBomb.False,
                PvP = P2PlayerType == EPlayerType.Human
            };
            
            await _context.Options.AddAsync(options);
            
            var boats = bs.Options.BoatOptions.GetBoats();
            var boatQt = Enumerable.Count(_context.Boats);

            foreach (var (boat, no) in boats)
            {
                var dbBoat = new Domain.Boat
                {
                    Name = boat.Name,
                    Size = boat.SizeOne
                };

                var addedAlready = false;

                if (boatQt > 0)
                {
                    foreach (var contextBoat in _context.Boats)
                    {
                        if (contextBoat.Name != dbBoat.Name || dbBoat.Name != "Carrier" &&
                            dbBoat.Name != "Battleship" &&
                            dbBoat.Name != "Submarine" &&
                            dbBoat.Name != "Cruiser" &&
                            dbBoat.Name != "Patrol") continue;
                        dbBoat = contextBoat;
                        addedAlready = true;
                        break;
                    }
                }

                if (!addedAlready)
                {
                    await _context.Boats.AddAsync(dbBoat);
                }

                var boatOption = new Domain.BoatOptions
                {
                    Options = options,
                    Boat = dbBoat,
                    Quantity = no
                };
                await _context.BoatOptions.AddAsync(boatOption);

                options.BoatOptions.Add(boatOption);
            }
            
            var p1 = new Player
            {
                Name = P1,
                EPlayerType = EPlayerType.Human
            };
            
            var p2 = new Player
            {
                Name = P2,
                EPlayerType = P2PlayerType == EPlayerType.Human ? EPlayerType.Human : EPlayerType.Ai
            };
            
            await _context.Players.AddAsync(p1);
            await _context.Players.AddAsync(p2);

            var game = new Game
            {
                Options = options,
                PlayerA = p1,
                PlayerB = p2,
                Lever = bs.GetLever(),
                WonBy = bs.WonBy(),
            };

            var board = new Board
            {
                Game = game,
                BoardState = serializedGame
            };

            game.Board = board;
            
            await _context.Games.AddAsync(game);
            
            await _context.SaveChangesAsync();
            
            p1.Game = game;
            p2.Game = game;

            await _context.SaveChangesAsync();

            return RedirectToPage(ShipGenerator ? "./NewDirectory1/Loading/" : "./NewDirectory1/PlanningBoss/", 
                new
                {
                    id = game.GameId,
                    q = 0,
                    c = P2PlayerType == EPlayerType.Human ? 0 : 1
                });
        }
    }
}