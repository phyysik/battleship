﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Domain.Enums;
using Head;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using BoatOptions = Head.BoatOptions;

namespace WebApp1.Pages.NewDirectory1
{
    
    public class PlanningBoss : PageModel
    {
        private readonly AppDbContext _context;
        private readonly ILogger<NewGame> _logger;

        public PlanningBoss(AppDbContext context, ILogger<NewGame> logger)
        {
            _context = context;
            _logger = logger;
        }
        
        public Game? Game { get; set; }
        public Battleship? Battleship { get; set; }
        
        public async Task<RedirectToPageResult> OnGetAsync(int id, int q, int c)
        {
            Game = await _context.Games
                .Where(g => g.GameId == id)
                .Include(o => o.Options)
                .FirstOrDefaultAsync();
            
            List<Domain.BoatOptions> boatOptionsList = new();

            foreach (var boatOptions in _context.BoatOptions
                .Include(b => b.Boat)
                .Include(o => o.Options)
                .Where(b => b.OptionsId == Game.OptionsId))

            {
                if (boatOptions.OptionsId == Game.OptionsId)
                {
                    boatOptionsList.Add(boatOptions);
                }
            }
            
            var boatDictionary = new Dictionary<string, (int, int)>();

            foreach (var boatOptions in boatOptionsList)
            {
                if (boatDictionary.ContainsKey(boatOptions.Boat.Name))
                {
                    boatDictionary[boatOptions.Boat.Name] =
                        (boatDictionary[boatOptions.Boat.Name].Item1,
                            boatDictionary[boatOptions.Boat.Name].Item2 + 1);
                }
                else
                {
                    boatDictionary.Add(boatOptions.Boat.Name,
                        (boatOptions.Boat.Size, boatOptions.Quantity));
                }
            }
            
            Battleship = new Battleship(new GameOptions(
                Game.Options.BoardWidth,
                Game.Options.BoardHeight,
                Game.Options.ContinueAfterGoodBomb == EContinueAfterGoodBomb.True,
                Game.Options.BoatContact switch
                {
                    EBoatContact.No => BoatContact.Disabled,
                    EBoatContact.Yes => BoatContact.Enabled,
                    EBoatContact.Hybrid => BoatContact.Hybrid,
                    _ => throw new ArgumentException()
                },
                Game.Options.PvP,
                new BoatOptions(boatDictionary)
            ));
            
            if (Game.Board?.BoardState != null) Battleship.SetSerializedGameState(Game.Board.BoardState);

            var boats = Battleship.Options.BoatOptions.GetBoats();

            if (q >= boats.Count) {

                if (c == 0 || c == 1)
                {
                    Game.Lever = !Game.Lever;

                    await _context.SaveChangesAsync();

                    if (c == 1)
                    {
                        return RedirectToPage("./Loading/", new
                        {
                            id
                        });
                    }
                    
                    return RedirectToPage("./PlanningBoss/", new
                    {
                        id,
                        q = 0,
                        c = 1
                    });
                }
            }

            var i = 0;
            
            var size = 0;
            foreach (var (boat, _) in boats)
            {
                if (q != i++) continue;
                size = boat.SizeOne;
                break;
            }
            
            if (size == 0)
            {
                throw new Exception();
            }

            return RedirectToPage("./Planning", new
            {
                id,
                size,
                q,
                c
            });
        }
    }
}