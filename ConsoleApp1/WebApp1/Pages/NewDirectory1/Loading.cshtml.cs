﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp1.Pages.NewDirectory1
{
    public class Loading : PageModel
    {
        public int Player { get; set; }
        [BindProperty]
        public int Id { get; set; }
        
        public void OnGetAsync(int id, int player)
        {
            Player = player;
            Id = id;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            return RedirectToPage("./Index/", new
            {
                id = Id
            });
        }
    }
}