﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Domain.Enums;
using Head;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using BoatOptions = Head.BoatOptions;

namespace WebApp1.Pages.NewDirectory1
{
    public class Index : PageModel
    {
        private readonly AppDbContext _context;
        private readonly ILogger<NewGame> _logger;

        public Index(AppDbContext context, ILogger<NewGame> logger)
        {
            _context = context;
            _logger = logger;
        }

        public Game? Game { get; set; }
        public Battleship? Battleship { get; set; }
        [BindProperty(SupportsGet = true)] public int PosX { get; set; }
        [BindProperty(SupportsGet = true)] public int PosY { get; set; }

        public async Task OnGetAsync(int id, string? dir)
        {
            Game = await _context.Games
                .Where(g => g.GameId == id)
                .Include(b => b.Board)
                .Include(o => o.Options)
                .FirstOrDefaultAsync();

            Battleship = new Battleship(new GameOptions(
                Game.Options.BoardWidth,
                Game.Options.BoardHeight,
                Game.Options.ContinueAfterGoodBomb == EContinueAfterGoodBomb.True,
                Game.Options.BoatContact switch
                {
                    EBoatContact.No => BoatContact.Disabled,
                    EBoatContact.Yes => BoatContact.Enabled,
                    EBoatContact.Hybrid => BoatContact.Hybrid,
                    _ => throw new ArgumentException()
                },
                Game.Options.PvP,
                new BoatOptions()
            ));
            
            var (wid, hei) = Battleship.Options.GetSize();

            switch (dir)
            {
                case "left":
                    PosX--;
                    break;
                case "right":
                    PosX++;
                    break;
                case "up":
                    PosY--;
                    break;
                case "down":
                    PosY++;
                    break;
            }

            if (PosX < 0)
            {
                PosX++;
            }
            else if (wid - 1 < PosX)
            {
                PosX--;
            }
            else if (PosY < 0)
            {
                PosY++;
            }
            else if (hei - 1 < PosY)
            {
                PosY--;
            }

            Battleship.SetSerializedGameState(Game.Board.BoardState);
            if (Game.Lever != Battleship.GetLever()) Battleship.SwitchLever();
        }

        public async Task<RedirectToPageResult> OnPostAsync(int id)
        {
            Game = await _context.Games
                .Where(g => g.GameId == id)
                .Include(b => b.Board)
                .Include(o => o.Options)
                .FirstOrDefaultAsync();

            Battleship = new Battleship(new GameOptions(
                Game.Options.BoardWidth,
                Game.Options.BoardHeight,
                Game.Options.ContinueAfterGoodBomb == EContinueAfterGoodBomb.True,
                Game.Options.BoatContact switch
                {
                    EBoatContact.No => BoatContact.Disabled,
                    EBoatContact.Yes => BoatContact.Enabled,
                    EBoatContact.Hybrid => BoatContact.Hybrid,
                    _ => throw new ArgumentException()
                },
                Game.Options.PvP,
                new BoatOptions()
            ));
            
            Battleship.SetSerializedGameState(Game.Board.BoardState);
            if (Game.Lever != Battleship.GetLever()) Battleship.SwitchLever();

            if (!Battleship.Bomb(PosX, PosY))
            {
                return RedirectToPage("./Index/", new
                {
                    PosX,
                    PosY,
                    id
                });
            }

            PosX = 0;
            PosY = 0;
            
            var continueTurn = Game.Lever == Battleship.GetLever();

            Game.Lever = Battleship.GetLever();
            Game.Board.BoardState = Battleship.GetSerializedGameState();
            Game.Description = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            _context.Games.Update(Game);
            await _context.SaveChangesAsync();

            if (continueTurn)
            {
                return RedirectToPage("./Index/", new
                {
                    PosX,
                    PosY,
                    id
                });
            }

            return RedirectToPage("./Loading/", new
            {
                id,
                player = Battleship.GetLever() ? 1 : 2
            });
        }
    }
}