﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp1.Pages
{
    public class LoadGame : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public LoadGame(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<Game> Game { get; set; } = null!;

        public async Task OnGetAsync()
        {
            Game = await _context.Games
                .Include(g => g.Options)
                .Include(g => g.PlayerA)
                .Include(g => g.PlayerB)
                .ToListAsync();
        }
    }
}