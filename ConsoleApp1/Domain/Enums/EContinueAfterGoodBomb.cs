namespace Domain.Enums
{
    // who has the right for next move after a successful move/hit
    public enum EContinueAfterGoodBomb
    {
        True,
        False
    }
}