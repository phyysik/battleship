﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace Domain
{
    public class Options
    {
        public int OptionsId { get; set; }

        [MaxLength(128)]
        public string Name { get; set; } = null!;

        public int BoardWidth { get; set; }
        public int BoardHeight { get; set; }

        // actually int in database, no extra work needed
        public EBoatContact BoatContact { get; set; }
        public EContinueAfterGoodBomb ContinueAfterGoodBomb { get; set; }
        public bool PvP { get; set; }

        public ICollection<BoatOptions> BoatOptions { get; set; } = null!;
    }
}