using System;

namespace Domain
{
    public class Board
    {
        public int BoardId { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        // serialised to json
        public string BoardState { get; set; } = null!;

        public int GameId { get; set; }
        public Game Game { get; set; } = null!;
    }
}