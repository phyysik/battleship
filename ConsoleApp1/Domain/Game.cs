using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Domain
{
    public class Game
    {
        public int GameId { get; set; }

        public int OptionsId { get; set; }
        public Options Options { get; set; } = null!;

        [MaxLength(512)]
        public string Description { get; set; } = DateTime.Now.ToString(CultureInfo.InvariantCulture);

        public int PlayerAId { get; set; }
        public Player PlayerA { get; set; } = null!;
        
        public int PlayerBId { get; set; }
        public Player PlayerB { get; set; } = null!;

        public Board Board { get; set; } = null!;

        public bool Lever { get; set; }
        
        public int WonBy { get; set; }

        public override string ToString()
        {
            return $"{Description}, {Options.BoardWidth}x{Options.BoardHeight}";
        }
    }
}