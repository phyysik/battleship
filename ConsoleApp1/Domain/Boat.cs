using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Boat
    {
        public int BoatId { get; set; }
        
        [DisplayName("Boat size")]
        [Range(1, int.MaxValue)]
        public int Size { get; set; }

        [DisplayName("Boat name")]
        [MaxLength(32)]
        public string Name { get; set; } = null!;

        public ICollection<BoatOptions> BoatOptions { get; set; } = null!;
    }
}