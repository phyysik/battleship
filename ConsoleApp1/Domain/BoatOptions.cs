using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class BoatOptions
    {
        public int BoatOptionsId { get; set; }
        
        [DisplayName("Boat quantity")]
        [Range(1, int.MaxValue)]
        public int Quantity { get; set; }
        
        public int BoatId { get; set; }
        public Boat Boat { get; set; } = null!;

        public int OptionsId { get; set; }
        public Options Options { get; set; } = null!;
    }
}