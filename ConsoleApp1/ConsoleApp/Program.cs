﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Domain.Enums;
using Error;
using GameBoard1;
using Head;
using MenuSystem1;
using Microsoft.EntityFrameworkCore;
using Boat = Head.Boat;
using BoatOptions = Head.BoatOptions;

namespace ConsoleApp1
{
    internal class Program
    {
        private static void Main()
        {
            var start = new Gaming();
            start.RunMainMenu();
        }

        private class Gaming
        {
            private Battleship _game = new(new GameOptions());
            private readonly Operations _ops = new();

            public void RunMainMenu()
            {
                var menuPlay = new Menu(MenuLevel.Lvl1);
                menuPlay.AddMenuItem(new MenuItem("New game", "1", NewGame));
                menuPlay.AddMenuItem(new MenuItem("Load game", "2", LoadGame));
                menuPlay.AddMenuItem(new MenuItem("Delete game", "3", DeleteSaveMenu));

                var menuMain = new Menu(MenuLevel.Lvl0);
                menuMain.AddMenuItem(new MenuItem("Play", "1", menuPlay.RunMenu));
                menuMain.AddMenuItem(new MenuItem("Options", "2", OptionsMenu));

                menuMain.RunMenu();
            }

            private string OptionsMenu()
            {
                var menuOptions = new Menu(MenuLevel.Lvl1);
                var (x, y) = _game!.Options.GetSize();
                var optionsChangeBoard = new MenuItem($"Change board size: ({x}, {y})", "1", DefaultMenuAction);
                optionsChangeBoard.ChangeExe(() =>
                {
                    ChangeBoardSize(optionsChangeBoard);
                    Console.Clear();
                    return "";
                });
                menuOptions.AddMenuItem(optionsChangeBoard);

                var optionsChangeContact =
                    new MenuItem($"Boat contact: {_game.Options.GetBoatContact()}", "2", DefaultMenuAction);
                optionsChangeContact.ChangeExe(() =>
                {
                    SwitchContact(optionsChangeContact);
                    Console.Clear();
                    return "";
                });
                menuOptions.AddMenuItem(optionsChangeContact);

                var optionsChangeAfterHit =
                    new MenuItem($"Continue shooting after successful bomb: {_game.Options.GetContinueAfterHit()}",
                        "3", DefaultMenuAction);

                menuOptions.AddMenuItem(optionsChangeAfterHit);
                optionsChangeAfterHit.ChangeExe(() =>
                {
                    SwitchAfterHitAction(optionsChangeAfterHit);
                    Console.Clear();
                    return "";
                });
                menuOptions.AddMenuItem(new MenuItem("Boat settings", "4", BoatMenu));
                menuOptions.AddMenuItem(new MenuItem("Reset all settings", "5", ResetOptions));

                return menuOptions.RunMenu();
            }

            private string BoatMenu()
            {
                var boats = _game!.Options.BoatOptions.GetBoats();
                var boatMenu = new Menu(MenuLevel.LvlN);

                var i = 0;
                foreach (var (boat, no) in boats)
                {
                    var temp = new MenuItem($"{boat} ({no})", (++i).ToString(), DefaultMenuAction);

                    var i1 = i - 1;
                    temp.ChangeExe(() =>
                    {
                        ChangeBoat(i1, temp);
                        return "";
                    });

                    boatMenu.AddMenuItem(temp);
                }

                boatMenu.AddMenuItem(new MenuItem("Add new boat", (1 + i).ToString(), AddNewBoat));

                return boatMenu.RunMenu();
            }

            private string ResetOptions()
            {
                _game.Options.ResetOptions();
                return "";
            }

            private string AddNewBoat()
            {
                Console.Clear();
                Console.WriteLine("Give the new boat a name:");
                Console.Write(">");

                var name = Console.ReadLine()?.Trim() ?? "";
                if (name == "")
                {
                    Console.WriteLine("Nothing entered. Returning back.");
                    return "";
                }

                var (ean, q, _) = Xy(false, "Enter ship size", "6");

                if (!ean || !_game.Options.BoatOptions.AddBoat(name, q))
                {
                    Console.WriteLine(
                        "If you entered something, a boat with this name already exists. Returning back.");
                    return "";
                }

                Console.WriteLine("New boat added!");

                return "b";
            }

            private void ChangeBoat(int which, MenuItem item)
            {
                var (ean, q, _) = Xy(false, "Enter quantity", "1");
                if (!ean) return;
                var boat = "";
                var i = 0;

                foreach (var (b0At, _) in _game.Options.BoatOptions.GetBoats())
                {
                    if (i == which)
                    {
                        boat = b0At.Name;
                    }

                    i++;
                }

                _game.Options.BoatOptions.ChangeBoatQuantity(boat, q);
                Boat boat2 = _game.Options.BoatOptions.GetBoat(boat)!;
                item.ChangeLabel($"{boat} ({boat2.SizeOne}x{boat2.SizeTwo}) ({q})");
            }

            private string BattleShip()
            {
                var action = new Menu(MenuLevel.LvlG);

                var continueGame = new MenuItem($"Continue (Player {(_game.GetLever() ? "1" : "2")})",
                    "1", DefaultMenuAction);

                action.AddMenuItem(continueGame);


                action.AddMenuItem(new MenuItem("Save game", "2", SaveSwitch));

                var undoMenu = new MenuItem("Undo previous move", "3", DefaultMenuAction, false);
                undoMenu.ChangeExe(() =>
                {
                    UndoGame(continueGame);
                    if (undoMenu.GetVisibility() && !_game.CanUndo()) undoMenu.ChangeVisibility();
                    return "";
                });

                action.AddMenuItem(undoMenu);

                continueGame.ChangeExe(() =>
                {
                    Move(continueGame);
                    if (!undoMenu.GetVisibility() && _game.CanUndo()) undoMenu.ChangeVisibility();
                    return "";
                });

                if (_game.IsWon()) Spam();
                var userChoice = action.RunMenu();
                return userChoice;
            }

            private void UndoGame(MenuItem item)
            {
                if (!_game.Undo()) return;
                item.ChangeLabel($"Continue (Player {(_game.GetLever() ? "1" : "2")})");
            }

            // too messy right now
            private void PlaceShips(IReadOnlyDictionary<Boat, int> boats)
            {
                foreach (var (boat, value) in boats)
                {
                    if (value < 1) continue;
                    for (var i = 0; i < value; i++)
                    {
                        var x = 0;
                        var y = 0;
                        var rotation = false;
                        var size = boat.SizeOne;

                        while (true)
                        {
                            Console.Clear();

                            var (wid, hei) = _game.Options.GetSize();

                            if (x < 0)
                            {
                                x++;
                            }
                            else if (x > wid - 1)
                            {
                                x--;
                            }
                            else if (y < 0)
                            {
                                y++;
                            }
                            else if (y > hei - 1)
                            {
                                y--;
                            }

                            if (!rotation)
                            {
                                while (x + size > wid)
                                {
                                    x--;
                                }
                            }
                            else
                            {
                                while (y + size > hei)
                                {
                                    y--;
                                }
                            }

                            PrintBoards(x, y, size, rotation);
                            Console.WriteLine("Use arrow keys to move around your ship and press enter to place it.");

                            var key = Console.ReadKey().Key;

                            switch (key)
                            {
                                case ConsoleKey.LeftArrow:
                                    x--;
                                    continue;
                                case ConsoleKey.RightArrow:
                                    x++;
                                    continue;
                                case ConsoleKey.UpArrow:
                                    y--;
                                    continue;
                                case ConsoleKey.DownArrow:
                                    y++;
                                    continue;
                                case ConsoleKey.Spacebar:
                                    if (!(rotation && y + size > hei || !rotation && x + size > wid))
                                    {
                                        rotation = !rotation;
                                    }

                                    continue;
                                case ConsoleKey.Enter:
                                    if (_game.CanPlaceShip(x, y, rotation, size))
                                    {
                                        _game.PlaceShip(x, y, rotation, size);
                                        break;
                                    }

                                    continue;
                                default:
                                    continue;
                            }

                            break;
                        }
                    }
                }
            }

            private void Move(MenuItem? item = null)
            {
                var (wid, hei) = _game.Options.GetSize();

                int x;
                int y;

                x = 0;
                y = 0;

                {
                    while (true)
                    {
                        Console.Clear();

                        if (x < 0)
                        {
                            x++;
                        }
                        else if (wid - 1 < x)
                        {
                            x--;
                        }
                        else if (y < 0)
                        {
                            y++;
                        }
                        else if (hei - 1 < y)
                        {
                            y--;
                        }

                        PrintBoards(x, y);
                        Console.WriteLine(
                            "Use arrow keys to move around enemy's grid and press enter to bomb. (Backspace to go back)");
                        Console.WriteLine("X - ship bombed, S - ship not bombed, O - bomb missed");

                        var key = Console.ReadKey().Key;

                        switch (key)
                        {
                            case ConsoleKey.LeftArrow:
                                x--;
                                continue;
                            case ConsoleKey.RightArrow:
                                x++;
                                continue;
                            case ConsoleKey.UpArrow:
                                y--;
                                continue;
                            case ConsoleKey.DownArrow:
                                y++;
                                continue;
                            case ConsoleKey.Enter:
                                if (!_game.Bomb(x, y))
                                {
                                    Console.WriteLine("Grid has already been shot.");
                                    continue;
                                }

                                break;
                            case ConsoleKey.Backspace:
                                break;
                            default:
                                continue;
                        }

                        break;
                    }
                }

                if (!_game.Options.IsPvp() && !_game.GetLever())
                {
                    var random = new Random();

                    while (!_game.GetLever())
                    {
                        do
                        {
                            x = random.Next(0, wid);
                            y = random.Next(0, hei);
                        } while (!_game.Bomb(x, y));
                    }
                }

                item?.ChangeLabel($"Continue (Player {(_game.GetLever() ? "1" : "2")})");
                Console.Clear();

                if (_game.IsWon())
                {
                    Spam();
                }
            }

            private void Spam()
            {
                Console.WriteLine($"Game has been won by player {_game.WonBy()}!\n" +
                                  $"Game has been won by player {_game.WonBy()}!\n" +
                                  $"Game has been won by player {_game.WonBy()}!\n" +
                                  $"Game has been won by player {_game.WonBy()}!\n" +
                                  $"Game has been won by player {_game.WonBy()}!");
            }

            private string NewGame()
            {
                Console.WriteLine("You are about to start a new game. Press Enter to confirm.");
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.Enter:
                        break;
                    default:
                        Console.WriteLine("Could not confirm. Returning to menu.");
                        return "";
                }

                int j;
                while (true)
                {
                    Console.Clear();
                    Console.WriteLine("PvP (1) or PvE (2)? Enter nothing to go back to menu.");
                    var input = Console.ReadLine()?.Trim() ?? "";

                    if (input == "") return "";

                    int i;
                    try
                    {
                        i = int.Parse(input);
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Nothing understood. Try again.");
                        continue;
                    }

                    Console.Clear();
                    Console.WriteLine("Do you want to place ships yourself (1) " +
                                      "or let ships be placed randomly (2)? Enter nothing to go to menu.");
                    var input2 = Console.ReadLine()?.Trim() ?? "";

                    if (input2 == "") return "";

                    try
                    {
                        j = int.Parse(input2);
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Nothing understood. Try again.");
                        continue;
                    }

                    switch (i)
                    {
                        case < 1:
                        case > 2:
                            continue;
                        case 1:
                            _game.Options.ChangePvp(true);
                            break;
                        default:
                            _game.Options.ChangePvp(false);
                            break;
                    }

                    break;
                }

                _game = new Battleship(_game.Options);

                if (j == 1) PlaceShips(_game.Options.BoatOptions.GetBoats());
                else _game.PlaceShipsRandomly();
                _game.SwitchLever();

                if (_game.Options.IsPvp() && j == 1) PlaceShips(_game.Options.BoatOptions.GetBoats());
                else _game.PlaceShipsRandomly();
                _game.SwitchLever();

                Console.Clear();

                return BattleShip();
            }

            private string DefaultMenuAction()
            {
                Console.WriteLine("Not implemented.");
                return "";
            }

            private void ChangeBoardSize(MenuItem item)
            {
                var (ean, x, y) = Xy(true, "Enter board size: (each axis minimum 4)",
                    "10,10", 40, 4, 16, 4);
                if (!ean) return;

                _game.Options.ChangeSize(x, y);
                _game = new Battleship(_game.Options);
                item.ChangeLabel($"Change board size: {x}, {y}");
            }

            private void SwitchAfterHitAction(MenuItem item)
            {
                _game.Options.SwitchContinueAfterHit();
                item.ChangeLabel($"Continue shooting after successful bomb: {_game.Options.GetContinueAfterHit()}");
            }

            private void SwitchContact(MenuItem item)
            {
                _game.Options.SwitchBoatCollision();
                item.ChangeLabel($"Boat contact: {_game.Options.GetBoatContact()}");
            }

            // return (x axis_coordinate, y axis_coordinate, bool is_successful
            // ean = a switch for different input requirements
            private (bool, int, int) Xy(bool ean, string prompt, string example,
                int maxX = 10, int minX = 1, int maxY = 10, int minY = 1)
            {
                int x;
                int y;

                while (true)
                {
                    Console.WriteLine($"{prompt} For example: {example}. Enter nothing to go back.");
                    Console.Write(">");
                    var input = Console.ReadLine()?.Trim() ?? "";
                    try
                    {
                        if (input == "") return (false, 0, 0);

                        var a = input.Split(",");

                        if (!ean)
                        {
                            return (true, int.Parse(a[0]), 0);
                        }

                        x = int.Parse(a[0]);
                        y = int.Parse(a[1]);

                        break;
                    } // ???????????????????????????????????
                    catch (FormatException)
                    {
                        Console.WriteLine($"Given invalid input: ({input}). Correct input example: {example}");
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Console.WriteLine($"Given invalid input: ({input}). Correct input example: {example}");
                    }
                    catch (GivenWrongInputException)
                    {
                        Console.WriteLine($"Given invalid input: ({input}). Correct input example: {example}");
                    }
                }

                return (true, x, y);
            }

            private void PrintBoards(int x, int y, int? big = null, bool? rotation = null)
            {
                if (big != null && rotation != null)
                {
                    Class1.DrawBoards(_game.GetBoard(true), _game.GetBoard(false), true, x, y, big, rotation);
                }
                else
                {
                    Class1.DrawBoards(_game.GetBoard(true), _game.GetBoard(false), false, x, y);
                }

                Console.WriteLine();
            }

            private int ChooseSave()
            {
                using var dbContext = new AppDbContext(_ops.DbOptions);

                var i = 1;

                var games = dbContext.Games;

                foreach (var game in games.Include(o => o.Options))
                {
                    Console.WriteLine($"({i++}) {game}");
                }

                Console.Write(">");
                var fileNo = Console.ReadLine()?.Trim() ?? "";

                try
                {
                    var fileNoInt = int.Parse(fileNo);
                    if (fileNoInt < 1 || fileNoInt >= i) throw new FormatException();
                    return fileNoInt;
                }
                catch (FormatException)
                {
                    throw new FormatException();
                }
            }

            private string LoadGame()
            {
                int gameId;

                try
                {
                    gameId = ChooseSave();

                }
                catch (FormatException)
                {
                    Console.WriteLine("Unable to detect save file with given input.");
                    return "";
                }
                
                using var dbContext = new AppDbContext(_ops.DbOptions);

                var game = _ops.LoadGame(gameId);

                var boatDictionary = new Dictionary<string, (int, int)>();

                foreach (var boatOptions in game.Options.BoatOptions)
                {
                    if (boatDictionary.ContainsKey(boatOptions.Boat.Name))
                    {
                        boatDictionary[boatOptions.Boat.Name] =
                            (boatDictionary[boatOptions.Boat.Name].Item1,
                                boatDictionary[boatOptions.Boat.Name].Item2 + 1);
                    }
                    else
                    {
                        boatDictionary.Add(boatOptions.Boat.Name,
                            (boatOptions.Boat.Size, boatOptions.Quantity));
                    }
                }

                var jsonBoard = game.Board.BoardState;

                _game = new Battleship(new GameOptions(
                    game.Options.BoardWidth,
                    game.Options.BoardHeight,
                    game.Options.ContinueAfterGoodBomb == EContinueAfterGoodBomb.True,
                    game.Options.BoatContact switch
                    {
                        EBoatContact.No => BoatContact.Disabled,
                        EBoatContact.Yes => BoatContact.Enabled,
                        EBoatContact.Hybrid => BoatContact.Hybrid,
                        _ => throw new ArgumentException()
                    },
                    game.Options.PvP,
                    new BoatOptions(boatDictionary)
                ));

                _game.SetSerializedGameState(jsonBoard);
                _game.SetWinner(game.WonBy);

                if (game.Lever != _game.GetLever()) _game.SwitchLever();

                // item.ChangeLabel($"Continue (Player {(_game.Lever ? "1" : "2")})");
                Console.Clear();
                Console.WriteLine("Loaded save file.");

                return BattleShip();
            }

            private string SaveSwitch()
            {
                Console.WriteLine("Create new save file or overwrite existing save file? " +
                                  "If chosen neither of given two options, saving will cancel.");
                Console.WriteLine("(1) Create new");
                Console.WriteLine("(2) Overwrite");

                var input = Console.ReadLine()?.Trim() ?? "";

                if (input != "1" && input != "2")
                {
                    Console.WriteLine("Saving cancelled.");
                    return "";
                }

                switch (input)
                {
                    case "1":
                        SaveGame();
                        break;
                    case "2":
                        //OverwriteSave();
                        break;
                }

                return "";
            }

            private void SaveGame()
            {
                var serializedGame = _game.GetSerializedGameState();
                
                using var dbContext = new AppDbContext(_ops.DbOptions);

                // dbContext.Database.EnsureDeleted();
                dbContext.Database.Migrate();

                var (item1, item2) = _game.Options.GetSize();
                var options = new Options
                {
                    BoardWidth = item1,
                    BoardHeight = item2,
                    BoatContact = (EBoatContact) _game.Options.GetBoatContact(),
                    ContinueAfterGoodBomb =
                        _game.Options.GetContinueAfterHit()
                            ? EContinueAfterGoodBomb.True
                            : EContinueAfterGoodBomb.False,
                    PvP = _game.Options.IsPvp(),
                    Name = "Untitled options"
                };

                dbContext.Options.Add(options);

                var boats = _game.Options.BoatOptions.GetBoats();
                var boatQt = Enumerable.Count(dbContext.Boats);

                foreach (var (boat, no) in boats)
                {
                    var dbBoat = new Domain.Boat
                    {
                        Name = boat.Name,
                        Size = boat.SizeOne
                    };

                    var addedAlready = false;

                    if (boatQt > 0)
                    {
                        foreach (var dbContextBoat in dbContext.Boats)
                        {
                            if (dbContextBoat.Name != dbBoat.Name || dbBoat.Name != "Carrier" &&
                                dbBoat.Name != "Battleship" &&
                                dbBoat.Name != "Submarine" &&
                                dbBoat.Name != "Cruiser" &&
                                dbBoat.Name != "Patrol") continue;
                            dbBoat = dbContextBoat;
                            addedAlready = true;
                            break;
                        }
                    }

                    if (!addedAlready)
                    {
                        dbContext.Boats.Add(dbBoat);
                    }

                    var boatOption = new Domain.BoatOptions
                    {
                        Options = options,
                        Boat = dbBoat,
                        Quantity = no
                    };
                    dbContext.BoatOptions.Add(boatOption);

                    options.BoatOptions ??= new List<Domain.BoatOptions>();
                    options.BoatOptions.Add(boatOption);
                }

                dbContext.SaveChanges();

                var p1 = new Player
                {
                    Name = "Player One",
                    EPlayerType = EPlayerType.Human
                };

                var p2 = new Player
                {
                    Name = "Player Two",
                    EPlayerType = _game.Options.IsPvp() ? EPlayerType.Human : EPlayerType.Ai
                };

                dbContext.Players.Add(p1);
                dbContext.Players.Add(p2);

                var game = new Game
                {
                    PlayerA = p1,
                    PlayerB = p2,
                    Lever = _game.GetLever(),
                    WonBy = _game.WonBy()
                };

                var board = new Board
                {
                    Game = game,
                    BoardState = serializedGame
                };

                game.Board = board;
                game.Options = options;

                dbContext.Games.Add(game);
                dbContext.SaveChanges();

                p1.GameId = game.GameId;
                p2.GameId = game.GameId;

                dbContext.SaveChanges();
            }

            private void OverwriteSave(int id)
            {
                var save = _ops.LoadGame(id);

                var serializedGame = _game.GetSerializedGameState();

                using var dbContext = new AppDbContext(_ops.DbOptions);
                
                // dbContext.Database.EnsureDeleted();
                dbContext.Database.Migrate();

                var (x, y) = _game.Options.GetSize();
                save!.Options.BoardHeight = y;
                save.Options.BoardWidth = x;
                save.Options.BoatContact = (EBoatContact) _game.Options.GetBoatContact();
                save.Options.ContinueAfterGoodBomb =
                    _game.Options.GetContinueAfterHit() ? EContinueAfterGoodBomb.True : EContinueAfterGoodBomb.False;
                save.Options.PvP = _game.Options.IsPvp();
                save.Options.Name = "name";
                dbContext.Options.Update(save.Options);
                dbContext.SaveChanges();

                save.PlayerA.Game = save;
                save.PlayerA.Name = "Player 1";
                save.PlayerB.Game = save;
                save.PlayerB.Name = "Player 2";
                save.PlayerB.EPlayerType = _game.Options.IsPvp() ? EPlayerType.Human : EPlayerType.Ai;
                dbContext.Players.Update(save.PlayerA);
                dbContext.Players.Update(save.PlayerB);
                dbContext.SaveChanges();

                save.Description = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                save.Lever = _game.GetLever();
                save.Board.BoardState = serializedGame;
                save.WonBy = _game.WonBy();
                dbContext.Games.Update(save);
                dbContext.SaveChanges();

                var bo = dbContext.BoatOptions.Where(b => b.OptionsId == save.Options.OptionsId);
                foreach (var boo in bo)
                {
                    dbContext.BoatOptions.Remove(boo);
                }

                dbContext.SaveChanges();

                var boats = _game.Options.BoatOptions.GetBoats();
                var boatQt = Enumerable.Count(dbContext.Boats);

                foreach (var (boat, no) in boats)
                {
                    var dbBoat = new Domain.Boat
                    {
                        Name = boat.Name,
                        Size = boat.SizeOne
                    };

                    var addedAlready = false;

                    if (boatQt > 0)
                    {
                        foreach (var dbContextBoat in dbContext.Boats)
                        {
                            if (dbContextBoat.Name != dbBoat.Name || dbBoat.Name != "Carrier" &&
                                dbBoat.Name != "Battleship" &&
                                dbBoat.Name != "Submarine" &&
                                dbBoat.Name != "Cruiser" &&
                                dbBoat.Name != "Patrol") continue;
                            dbBoat = dbContextBoat;
                            addedAlready = true;
                            break;
                        }
                    }

                    if (!addedAlready)
                    {
                        dbContext.Boats.Add(dbBoat);
                    }

                    var boatOption = new Domain.BoatOptions
                    {
                        Options = save.Options,
                        Boat = dbBoat,
                        Quantity = no
                    };
                    dbContext.BoatOptions.Add(boatOption);
                    save.Options.BoatOptions.Add(boatOption);
                }

                dbContext.SaveChanges();
            }
            
            private string DeleteSaveMenu()
            {
                int game;
                try
                {
                    game = ChooseSave();
                }
                catch (FormatException)
                {
                    Console.WriteLine("rip");
                    return "";
                }

                DeleteSave(game);
                return "";
            }

            private async void DeleteSave(int id)
            {
                await using var dbContext = new AppDbContext(_ops.DbOptions);
    
                var game = dbContext.Games
                    .Where(g => g.GameId == id)
                    .Include(g => g.Options)
                    .Include(g => g.PlayerA)
                    .Include(g => g.PlayerB)
                    .FirstOrDefaultAsync().Result;

                dbContext.Remove(game);

                await dbContext.SaveChangesAsync();
            }
        }
    }
}