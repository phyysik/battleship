﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MenuSystem1
{
    public enum MenuLevel
    {
        Lvl0,
        Lvl1,
        LvlN,
        LvlG
    }

    public class Menu
    {
        private Dictionary<string, MenuItem> MenuItems { get; } = new();
        private readonly MenuLevel _menuLevel;

        public Menu(MenuLevel level)
        {
            _menuLevel = level;
        }

        private bool IsReturnCmd(string cmd)
        {
            return cmd == "x" ||
                   cmd == "m" && _menuLevel != MenuLevel.Lvl0 ||
                   cmd == "b" && _menuLevel == MenuLevel.LvlN;
        }

        public void AddMenuItem(MenuItem item)
        {
            var cmd = item.GetCommand();
            if (cmd.Length < 1 || cmd.Length > 8)
            {
                throw new ArgumentException($"Invalid MenuItem Command length ({cmd.Length}). " +
                                            "Valid length: 1-8.");
            }
            if (!IsReturnCmd(cmd))
            {
                MenuItems.Add(cmd, item);
            }
            else
            {
                throw new ArgumentException($"MenuItem with Command ({cmd}) already exists.");
            }
        }

        public string RunMenu()
        {
            string userChoice;
            
            do
            {
                foreach (var menuItem in MenuItems.Where(menuItem => menuItem.Value.GetVisibility()))
                {
                    Console.WriteLine(menuItem.Value);
                }

                switch (_menuLevel)
                {
                    case MenuLevel.Lvl0:
                        Console.WriteLine("(x) Exit game");
                        break;
                    case MenuLevel.Lvl1:
                        Console.WriteLine("(m) Main menu");
                        Console.WriteLine("(x) Exit game");
                        break;
                    case MenuLevel.LvlN:
                        Console.WriteLine("(b) Back");
                        Console.WriteLine("(m) Main menu");
                        Console.WriteLine("(x) Exit game");
                        break;
                    case MenuLevel.LvlG :
                        Console.WriteLine("(m) Main menu");
                        break;
                    default:
                        throw new Exception("Unknown menu depth.");
                }
                
                Console.Write(">");

                userChoice = Console.ReadLine()?.ToLower().Trim() ?? "";
                Console.Clear();
                
                if (!IsReturnCmd(userChoice))
                {
                    
                    if (MenuItems.TryGetValue(userChoice, out var userMenuItem) && userMenuItem.GetVisibility())
                    {
                        var exe = userMenuItem.GetExe();
                        userChoice = exe();
                    }
                    else
                    {
                        Console.WriteLine("Not an option.");
                    }
                }

                if (!IsReturnCmd(userChoice)) continue;
                if (_menuLevel == MenuLevel.Lvl0)
                {
                    Console.WriteLine("Goodbye.");
                }

                if (userChoice == "b") userChoice = "";
                break;

            } while (true);

            return userChoice;
        }
    }
}
