using System;

namespace MenuSystem1
{
    public sealed class MenuItem
    {
        private string Label { get; set; }

        private string Command { get; }
        private Func<string> Exe { get; set; }
        private bool Visible { get; set; }

        public MenuItem(string label, string command, Func<string> exe)
        {
            Label = label.Trim();
            Command = command.ToLower().Trim();
            Exe = exe;
            Visible = true;
        }

        public MenuItem(string label, string command, Func<string> exe, bool visible)
        {
            Label = label.Trim();
            Command = command.ToLower().Trim();
            Exe = exe;
            Visible = visible;
        }

        public bool GetVisibility()
        {
            return Visible;
        }

        public void ChangeVisibility()
        {
            Visible = !Visible;
        }

        public override string ToString()
        {
            return $"({Command}) {Label}";
        }

        public void ChangeLabel(string s)
        {
            Label = s;
        }

        public Func<string> GetExe()
        {
            return Exe;
        }

        public void ChangeExe(Func<string> exe)
        {
            Exe = exe;
        }

        public string GetCommand()
        {
            return Command;
        }
    }
}