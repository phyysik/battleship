﻿using System;
using Head;

namespace GameBoard1
{
    public static class Class1
    {
        private static void DrawXNav(int width)
        {
            var j = 'A';
            Console.Write("   ");
            for (var i = 0; i < width; i++)
            {
                while (j > 'Z' && j < 'a')
                {
                    j++;
                }
                
                Console.Write($"  {j++} ");
            }
        }

        private static void DrawXSeparator(int width)
        {
            Console.Write("   ");
            
            for (var col = 0; col < width; col++)
            {
                Console.Write("+---");
            }
            Console.Write("+");
        }

        private static void DrawBoardSpace()
        {
            Console.Write("     ");
        }

        private static void DrawBoardLine(bool ean, bool opponent, int row, int width, 
            CellState[,] board, int? x, int? y, int? big, bool? rotation)
        {
            var filler = row < 9 ? " " : "";
            Console.Write($"{filler}{row + 1} ");
            for (var col = 0; col < width; col++)
            {
                Console.Write("|");

                if (CellMagic(board[col, row], opponent) == "S")
                {
                    Console.BackgroundColor = ConsoleColor.DarkBlue;
                }
                else if (CellMagic(board[col, row], opponent) == "X")
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                }
                else if (CellMagic(board[col, row], opponent) == "O")
                {
                    Console.BackgroundColor = ConsoleColor.White;
                }
                    
                if (big == null && rotation == null && x != null && y != null && x == col && y == row && !ean && opponent ||
                    x != null && y != null && rotation != null && big != null && ean && !opponent && 
                    (rotation == false && col < x + big && col > x - 1 && row == y ||
                     rotation == true && row < y + big && row > y - 1 && col == x))
                {
                    Console.BackgroundColor = ConsoleColor.Yellow;
                }
                Console.Write($" { CellMagic(board[col, row], opponent) } ");
                Console.ResetColor();
            }
            Console.Write("|");
        }

        private static void DrawBanners(int width)
        {
            Console.Write("   ");
            Console.Write("YOU");
            for (var i = 0; i < 4 * width + 1; i++)
            {
                Console.Write(" ");
            }
            
            DrawBoardSpace();
            Console.Write("ENEMY");
        }

        public static void DrawBoards(CellState[,] board1, CellState[,] board2, bool ean,
            int? x = null, int? y = null, int? big = null, bool? rotation = null)
        {
            var width = board1.GetUpperBound(0) + 1;
            var height = board1.GetUpperBound(1) + 1;
            var opponent = false;
            
            DrawBanners(width);
            Console.WriteLine();
            
            DrawXNav(width);
            DrawBoardSpace();
            DrawXNav(width);
            Console.WriteLine();
            
            DrawXSeparator(width);
            DrawBoardSpace();
            DrawXSeparator(width);
            Console.WriteLine();
            
            for (var row = 0; row < height; row++)
            {
                DrawBoardLine(ean, opponent, row, width, board1, x, y, big, rotation);
                DrawBoardSpace();
                opponent = !opponent;

                DrawBoardLine(ean, opponent, row, width, board2, x, y, big, rotation);
                Console.WriteLine();
                opponent = !opponent;
                
                DrawXSeparator(width);
                DrawBoardSpace();
                DrawXSeparator(width);
                Console.WriteLine();
            }
        }
        
        public static string CellMagic(CellState cellState, bool opponent)
        {
            if (cellState == CellState.Reserved || cellState == CellState.Ship && opponent)
            {
                cellState = CellState.Empty;
            }
            
            return cellState switch
            {
                CellState.Empty => " ",
                CellState.Ship => "S",
                CellState.BombedEmpty => "O",
                CellState.BombedShip => "X",
                _ => "FAIL"
            };
        }
    }
}