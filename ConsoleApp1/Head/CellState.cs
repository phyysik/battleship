namespace Head
{
    public enum CellState
    {
        Empty,
        Ship,
        BombedEmpty,
        BombedShip,
        Reserved
    }
}