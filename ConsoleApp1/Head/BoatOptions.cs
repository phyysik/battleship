﻿using System;
using System.Collections.Generic;

namespace Head
{
    public class BoatOptions
    {
        
        private readonly Dictionary<Boat, int> _boatsDictionary = new();

        public BoatOptions()
        {
            _boatsDictionary.Add(new Boat("Carrier", 5), 1);
            _boatsDictionary.Add(new Boat("Battleship", 4), 1);
            _boatsDictionary.Add(new Boat("Submarine", 3), 1);
            _boatsDictionary.Add(new Boat("Cruiser", 2), 1);
            _boatsDictionary.Add(new Boat("Patrol", 1), 1);
        }

        public BoatOptions(Dictionary<string, (int, int)> boats)
        {
            foreach (var (key, (item1, item2)) in boats)
            {
                _boatsDictionary.Add(new Boat(key, item1), item2);
            }
        }

        public Dictionary<Boat, int> GetBoats()
        {
            return _boatsDictionary;
        }

        public Boat? GetBoat(string name)
        {
            foreach (var (boat, _) in _boatsDictionary)
            {
                if (name == boat.Name) return boat;
            }

            return null;
        }

        // returns -1 if boat was not found
        public int GetOneBoatQuantity(string name)
        {
            foreach (var (boat, no) in _boatsDictionary)
            {
                if (name == boat.Name) return no;
            }

            return -1;
        }
        
        public bool AddBoat(string name, int size)
        {
            if (GetBoat(name) != null) return false;
            
            _boatsDictionary.Add(new Boat(name, size), 1);

            return true;
        }

        public bool ChangeBoatQuantity(string name, int i)
        {
            if (i < 0) return false;
            
            foreach (var (key, _) in _boatsDictionary)
            {
                if (key.Name != name) continue;
                _boatsDictionary[key] = i;
                return true;
            }

            return false;
        }

        public bool DeleteBoat(string name)
        {
            foreach (var (key, _) in _boatsDictionary)
            {
                if (key.Name == name)
                {
                     return _boatsDictionary.Remove(key);
                }
            }

            return false;
        }
    }
}