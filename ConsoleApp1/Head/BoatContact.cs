namespace Head
{
    public enum BoatContact
    {
        Disabled,
        Enabled,
        Hybrid
    }
}