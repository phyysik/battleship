﻿namespace Head
{
    public class Boat
    {
        public Boat(string name, int size1)
        {
            Name = name;
            SizeOne = size1;
            SizeTwo = 1;
        }

        public string Name { get; set; }
        public int SizeOne { get; set; }
        public int SizeTwo { get; set; }

        public override string ToString()
        {
            return $"{Name} ({SizeOne}x{SizeTwo})";
        }
    }
}