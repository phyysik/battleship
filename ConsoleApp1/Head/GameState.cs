namespace Head
{
    public class GameState
    {
        public CellState[,] Board1 { get; set; }
        public CellState[,] Board2 { get; set; }
        public bool Lever { get; set; }
        public int WonBy { get; set; }

        public GameState(CellState[,] b1, CellState[,] b2, bool lever, int wonBy)
        {
            Board1 = b1;
            Board2 = b2;
            Lever = lever;
            WonBy = wonBy;
        }
    }
}