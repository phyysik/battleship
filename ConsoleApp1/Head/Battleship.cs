﻿using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Head
{
    public class Battleship
    {
        private CellState[,] _board1;
        private CellState[,] _board2;
        private int _wonBy;
        public GameOptions Options;
        private readonly Stack<GameState> _gameStates = new();
        public Battleship()
        {
            Options = new GameOptions();
            var (x, y) = Options.GetSize();
            _board1 = new CellState[x, y];
            _board2 = new CellState[x, y];
            _wonBy = 0;
        }

        public Battleship(GameOptions options)
        {
            Options = options;
            var (x, y) = Options.GetSize();
            _board1 = new CellState[x, y];
            _board2 = new CellState[x, y];
            _wonBy = 0;
        }
        
        // ean: basic switch when choosing a board.
        // ean == true { return board got with lever }
        // ean == false { return board got with inverted lever }
        public CellState[,] GetBoard(bool ean)
        {
            var (x, y) = Options.GetSize();
            var copy = new CellState[x, y];
            CellState[,] board = (ean ? GetLever() : !GetLever()) ? _board1 : _board2;
            Array.Copy(board, copy, _board1.Length);
            return copy;
        }

        // false == p1's turn; true == p2's turn
        private bool Lever { get; set; } = true;

        public bool GetLever()
        {
            return Lever;
        }

        public void SwitchLever()
        {
            Lever = !Lever;
        }

        private bool BasicCheck(int x, int y)
        {
            var (width, height) = Options.GetSize();
            return x < width && y < height && x >= 0 && y >= 0;
        }

        public bool Bomb(int x, int y)
        {
            CellState[,] board = GetLever() ? _board2 : _board1;

            if (!BasicCheck(x, y) || board[x, y] == CellState.BombedShip || board[x, y] == CellState.BombedEmpty) return false;
            
            _gameStates.Push(new GameState(GetBoard(GetLever()), GetBoard(!GetLever()), GetLever(), _wonBy));
            
            if (board[x, y] == CellState.Ship)
            {
                board[x, y] = CellState.BombedShip;
                
                if (_wonBy == 0) WinnerCheck();
                if (Options.GetContinueAfterHit())
                {
                    return true;
                }
            }
            else
            {
                board[x, y] = CellState.BombedEmpty;
            }

            SwitchLever();
            
            return true;
        }

        public bool CanUndo()
        {
            return _gameStates.Count > 0;
        }

        public bool Undo()
        {
            if (!CanUndo()) return false;
            
            if (!Options.IsPvp())
            {
                while (!_gameStates.Peek().Lever) _gameStates.Pop();
            }

            SetState(_gameStates.Pop());
            return true;
        }

        private void SetState(GameState state)
        {
            _board1 = state.Board1;
            _board2 = state.Board2;
            if (state.Lever != Lever) SwitchLever();
            _wonBy = state.WonBy;
        }

        private void WinnerCheck()
        {
            CellState[,] board = GetLever() ? _board2 : _board1;
            var (width, height) = Options.GetSize();
            
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    if (board[x, y] == CellState.Ship)
                    {
                        return;
                    }
                }
            }
            
            _wonBy = board == _board1 ? 2 : 1;
        }

        public bool IsWon()
        {
            return _wonBy != 0;
        }

        public int WonBy()
        {
            return _wonBy;
        }

        public void SetWinner(int i)
        {
            if (i < 0 || i > 2) return;
            _wonBy = i;
        }

        // rotation false = horizontal, rotation true = vertical
        // x and y always (top/left) end of ship
        public bool CanPlaceShip(int x, int y, bool rotation, int big)
        {
            CellState[,] board = GetLever() ? _board1 : _board2;
            
            if (board[x, y] != CellState.Empty) return false;
            
            if (!rotation)
            {
                if (!BasicCheck(x + big - 1, y)) return false;
                for (var i = x; i < x + big; i++)
                {
                    if (board[i, y] != CellState.Empty)
                    {
                        return false;
                    }
                }
            }
            else
            {
                if (!BasicCheck(x, y + big - 1)) return false;
                for (var i = y; i < y + big; i++)
                {
                    if (board[x, i] != CellState.Empty)
                    {
                        return false;
                    }
                }
            }
            
            return true;
        }

        public void PlaceShip(int x, int y, bool rotation, int big)
        {
            if (!CanPlaceShip(x, y, rotation, big)) return;
            
            CellState[,] board = GetLever() ? _board1 : _board2;

            if (!rotation)
            {
                for (var i = x; i < x + big; i++)
                {
                    ConstructShip(i, y, board);
                }
            }
            else
            {
                for (var i = y; i < y + big; i++)
                {
                    ConstructShip(x, i, board);
                }
            }
        }

        private void ConstructShip(int x, int y, CellState[,] board)
        {
            board[x, y] = CellState.Ship;

            if (Options.GetBoatContact() == BoatContact.Enabled) return;
            
            ReserveEnvironment(x - 1, y, board);
            ReserveEnvironment(x + 1, y, board);
            ReserveEnvironment(x, y - 1, board);
            ReserveEnvironment(x, y + 1, board);

            if (Options.GetBoatContact() != BoatContact.Disabled) return;
            
            ReserveEnvironment(x - 1, y - 1, board);
            ReserveEnvironment(x + 1, y - 1, board);
            ReserveEnvironment(x + 1, y + 1, board);
            ReserveEnvironment(x - 1, y + 1, board);
        }

        private void ReserveEnvironment(int x, int y, CellState[,] board)
        {
            var (width, height) = Options.GetSize();
            
            if (x >= 0 && x < width && y >= 0 && y < height && board[x, y] != CellState.Ship)
            {
                board[x, y] = CellState.Reserved;
            }
        }
        
        public void SetSerializedGameState(string json)
        {
            var state = JsonSerializer.Deserialize<Boards>(json);
            var (width, height) = Options.GetSize();
            
            _board1 = JaggedToMultidimensional(state!.Board1, width, height);
            _board2 = JaggedToMultidimensional(state!.Board2, width, height);
        }
        
        private static CellState[,] JaggedToMultidimensional(CellState[][] boardFrom, int width, int height)
        {
            var boardTo = new CellState[width, height];
            
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    boardTo[x, y] = boardFrom[x][y];
                }
            }
            
            return boardTo;
        }

        public string GetSerializedGameState()
        {
            var boardWidth = _board1.GetLength(0);
            var boardHeight = _board1.GetLength(1);

            var state = new Boards
            {
                Board1 = MultidimensionalToJagged(_board1, boardWidth, boardHeight),
                Board2 = MultidimensionalToJagged(_board2, boardWidth, boardHeight)
            };
            
            var jsonOptions = new JsonSerializerOptions();

            return JsonSerializer.Serialize(state, jsonOptions);
        }

        private static CellState[][] MultidimensionalToJagged(CellState[,] boardFrom, int width, int height)
        {
            var boardTo = new CellState[width][];
            
            for (var i = 0; i < boardTo.Length; i++)
            {
                boardTo[i] = new CellState[height];
            }

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    boardTo[x][y] = boardFrom[x, y];
                }
            }
            
            return boardTo;
        }
        
        public void PlaceShipsRandomly()
        {
            var (wid, hei) = Options.GetSize();

            foreach (var (boat, value) in Options.BoatOptions.GetBoats())
            {
                if (value < 1) continue;

                for (var i = 0; i < value; i++)
                {
                    int x;
                    int y;
                    bool rotation;
                    var size = boat.SizeOne;
                    var random = new Random();

                    do
                    {
                        x = random.Next(wid);
                        y = random.Next(hei);
                        rotation = random.Next(2) != 0;
                        Console.WriteLine("CALCULATING...\nYEP...");
                    } while (!CanPlaceShip(x, y, rotation, size));

                    PlaceShip(x, y, rotation, size);
                }
            }
        }
    }
}