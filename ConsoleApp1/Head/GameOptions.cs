namespace Head
{
    public class GameOptions
    {
        private int BoardWidth { get; set; }
        private int BoardHeight { get; set; }
        private bool ContinueAfterHit { get; set; }
        private BoatContact BoatContact { get; set; }
        private bool PvP { get; set; }
        public BoatOptions BoatOptions { get; set; }
        
        public GameOptions()
        {
            BoardWidth = 10;
            BoardHeight = 10;
            ContinueAfterHit = true;
            BoatContact = BoatContact.Disabled;
            BoatOptions = new BoatOptions();
            PvP = true;
        }

        public GameOptions(int x, int y, bool continueAfterHit, BoatContact boatContact, bool pvp,
            BoatOptions boatOptions)
        {
            BoardWidth = x;
            BoardHeight = y;
            ContinueAfterHit = continueAfterHit;
            BoatContact = boatContact;
            PvP = pvp;
            BoatOptions = boatOptions;
        }

        public void ResetOptions()
        {
            BoardWidth = 10;
            BoardHeight = 10;
            ContinueAfterHit = true;
            BoatContact = BoatContact.Disabled;
            BoatOptions = new BoatOptions();
            PvP = true;
        }

        public (int, int) GetSize()
        {
            return (BoardWidth, BoardHeight);
        }

        public bool GetContinueAfterHit()
        {
            return ContinueAfterHit;
        }

        public BoatContact GetBoatContact()
        {
            return BoatContact;
        }

        public void ChangeSize(int x, int y)
        {
            BoardWidth = x;
            BoardHeight = y;
        }

        public void SwitchContinueAfterHit()
        {
            ContinueAfterHit = !ContinueAfterHit;
        }

        public void SwitchBoatCollision()
        {
            if (BoatContact == BoatContact.Hybrid)
            {
                BoatContact = BoatContact.Disabled;
            }
            else
            {
                BoatContact++;
            }
        }

        public bool IsPvp()
        {
            return PvP;
        }

        public void ChangePvp(bool newPvp)
        {
            PvP = newPvp;
        }
    }
}